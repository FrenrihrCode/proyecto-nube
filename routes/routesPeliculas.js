const express = require('express');
const Peliculas = require('../models/peliculas');
const Usuario = require('../models/usuario');
const app = express();
const {verificaToken} = require("../middlewares/authentication")
const fileUpload = require("express-fileupload");
//fs and aws settings
const AWS = require('aws-sdk');
const s3Client = new AWS.S3({
    accessKeyId: process.env.AWS_ACCESS_KEY,
    secretAccessKey: process.env.AWS_SECRET_ACCESS_KEY,
    region: process.env.REGION
});
const uploadParams = {
         Bucket: 's3mancor',
         Key: '', // pass key
         Body: null, // pass file body
};

//default options
app.use(fileUpload({ useTempFiles: true}));

//manejar archivo
const fs = require('fs')
const path = require('path')

if (typeof localStorage === "undefined" || localStorage === null) {
    var LocalStorage = require('node-localstorage').LocalStorage;
    localStorage = new LocalStorage('./scratch');
  }


app.get("/pelicula", async function (req, res) {
    const peliculas = await Peliculas.find({});
    res.render('pelicula', { peliculas })
});
app.get("/pelicula/categoria/:categoria", async function (req, res) {
    const categoria = req.params.categoria
    const peliculas = await Peliculas.find({categoria:req.params.categoria});
    res.render('pelicula', {peliculas: peliculas, categoria:categoria})
});
app.get('/agregarPelicula/',verificaToken, async function(req, res){
    let error = [];
    let titulo = "";
    let descripcion = "";
    let categoria ="";
    let tag="";
    let tag1="";
    let tag2="";
    let errorextension="";
    res.render('agregarPelicula', {error,titulo,descripcion,categoria,tag2,tag1,tag},errorextension)
});
app.post('/pelicula_crear',verificaToken, async function(req, res) {
    const { titulo, descripcion, categoria, tag, tag1, tag2 } = req.body;
    let tags = [];
    tags.push(tag);

    if(tag1) tags.push(tag1)
    if(tag2) tags.push(tag2)
    //console.log(titulo)
    //console.log(req.files)

    if (!(req.files)) {
        return res.status(400).json({
            ok: false,
            err: {
                message: "No se ha seleccionado ningun archivo",
            },
        });
    }
   

    //extension
    let archivo = req.files.image;

    let nombreArchivoCortado = archivo.name.split(".");
    let extension = nombreArchivoCortado[nombreArchivoCortado.length - 1];
    
    //extensiones permitidas
    let extensionesValidas = ["png","jpg","gif","jpeg"];

    if (extensionesValidas.indexOf(extension) < 0) {
        let error = [];
        let errorextension ="Formato de archivo no valido"
        return res.render("agregarPelicula",{error,titulo,descripcion,categoria,tag2,tag1,tag, errorextension});
    }
    //Nombre archivo
    let nombreArchivo = `${titulo.replace( /[^-A-Za-z0-9]+/g, '-' ).toLowerCase()}-${new Date().getMilliseconds()}.${extension}`;
    /*
    await archivo.mv(`public/assets/uploads/peliculas/${nombreArchivo}`, (err) => {
        if (err) {
            return res.status(500).json({
                ok: false,
                err,
            });
        }

    });
    */
    await archivo.mv(`public/assets/img/${nombreArchivo}`, (err) => {
        if (err) {
            return res.status(500).json({
                ok: false,
                err,
            });
        }
        let imagenS3 = path.resolve(__dirname, `../public/assets/img/${nombreArchivo}`);
    
        fs.readFile( imagenS3, function (err, data) {
            if (err) {
                console.log('Ocurrio un error:', err)
            } // Something went wrong!
            else {
                const params = uploadParams;
                uploadParams.Key = `manu/${nombreArchivo}`; //aqui va el nombre del archivo
                uploadParams.Body = data; //aqui va el path osea la ruta del archive pes
                s3Client.upload(params, async (err, data) => {
                    if (err) {
                        console.log('Ocurrio un error:', err);
                        res.redirect("/pelicula");
                    } else {
                        let newPelicula = new Peliculas({ titulo, descripcion, image: data.Location, categoria, tags });
                        await newPelicula.save(function(err){
                            if( err ) console.log(err);
                        });
                        res.redirect("/pelicula");
                    }
                    fs.unlink(imagenS3, (err) => {
                        if (err) {
                            console.log('Error de remover: ', err.message)
                        }
                    })
                });
            }
        })
    })
    /*
    let newPelicula = new Peliculas({ titulo, descripcion, image: nombreArchivo, categoria, tags });
    await newPelicula.save(function(err){
        if( err ) console.log(err);
    });*/
});
app.get("/pelicula/:id", async function (req, res) {
    const pelicula = await Peliculas.findById(req.params.id);
    res.render('pelicula-single', {pelicula: pelicula})
});
app.post('/pelicula/:id/comentar',  async function(req, res) {
    const { comentario } = req.body;
    const noticia = await Peliculas.findById(req.params.id);
    let img_usuario = 'usuario.png';
    let nombre_usuario = localStorage.getItem('name');
    noticia.comentarios.push({ nombre_usuario, img_usuario, comentario });
    await noticia.save(function(err){
        if( err ) console.log(err);
    });
    res.redirect("/pelicula/" + req.params.id);
});


app.get('/editPelicula/:id',verificaToken, async function(req, res){
    let error = [];
    let errorextension = ""
    const usersName = await Usuario.find({});
    const pelicula = await Peliculas.findById(req.params.id);
    res.render('editPelicula', {error, pelicula, usersName, errorextension, id:req.params.d })
});
app.put('/updatepelicula/:id',verificaToken, async function(req, res){
    const {id2, titulo,descripcion,imageAnterior,categoria,tag,tag1,tag2 } = req.body;
    let id= req.params.id
    let tags = [];
    tags.push(tag);
    if(tag1) tags.push(tag1)
    if(tag2) tags.push(tag2)

    if (!(req.files)) {
        if(id2 === 'undefined'){
            await Peliculas.findByIdAndUpdate(id, { titulo: titulo,descripcion: descripcion, image: imageAnterior,categoria:categoria,tags:tags });
        }else {
            await Peliculas.findByIdAndUpdate(id2, { titulo: titulo,descripcion: descripcion, image: imageAnterior,categoria:categoria,tags:tags });    
        }
        return res.redirect("/pelicula");
    }

    //extension
    let archivo = req.files.image;
    let nombreArchivoCortado = archivo.name.split(".");
    let extension = nombreArchivoCortado[nombreArchivoCortado.length - 1];
    //extensiones permitidas
    let extensionesValidas = ["png","jpg","gif","jpeg"];
    if (extensionesValidas.indexOf(extension) < 0) {
        let error = [];
        let errorextension = "Formato de archivo no valido"
        const usersName = await Usuario.find({});
        const pelicula = Peliculas({titulo, descripcion, image: imageAnterior, categoria, tags });
        return res.render('editPelicula', {error, pelicula, usersName, errorextension,id })
    }
    //Nombre archivo
    let nombreArchivo = `${titulo.replace( /[^-A-Za-z0-9]+/g, '-' ).toLowerCase()}-${new Date().getMilliseconds()}.${extension}`;
    
    await archivo.mv(`public/assets/img/${nombreArchivo}`, (err) => {
        if (err) {
            return res.status(500).json({
                ok: false,
                err,
            });
        }
        let imagenS3 = path.resolve(__dirname, `../public/assets/img/${nombreArchivo}`);
    
        fs.readFile( imagenS3, function (err, data) {
            if (err) {
                console.log('Ocurrio un error:', err)
            } // Something went wrong!
            else {
                const params = uploadParams;
                uploadParams.Key = `manu/${nombreArchivo}`; //aqui va el nombre del archivo
                uploadParams.Body = data; //aqui va el path osea la ruta del archive pes
                s3Client.upload(params, async (err, data) => {
                    if (err) {
                        console.log('Ocurrio un error:', err);
                        res.redirect("/pelicula");
                    } else {
                        if(id2 === 'undefined'){
                            imagenUsuario(id,titulo,descripcion,categoria,tags, res, data.Location);
                        }else {
                            imagenUsuario(id2,titulo,descripcion,categoria,tags, res, data.Location);
                        }
                        res.redirect("/pelicula");
                    }
                    fs.unlink(imagenS3, (err) => {
                        if (err) {
                            console.log('Error de remover: ', err.message)
                        }
                    })
                });
            }
        })
    });
    //await Peliculas.findByIdAndUpdate(req.params.id, { titulo: titulo,descripcion: descripcion, image: nombreArchivo,categoria:categoria,tags:tags });
})
app.delete('/deletePelicula/:id',verificaToken, async function(req, res) {
    let id = req.params.id;
    const peliculas = await Peliculas.findById(id);
    console.log(peliculas.image)
    await Peliculas.findByIdAndDelete(id);
    res.redirect("/pelicula");
});

function imagenUsuario(id,titulo,descripcion,categoria,tags, res, nombreArchivo) {
    Peliculas.findById(id, (err, usuarioDB) => {
        if(err) {
            return res.status(500).json({
                ok: false,
                err,
            });
        }

        if (!usuarioDB) {
            return res.status(400).json({
                ok:false,
                err: {
                    message :
                    "Usuario no existe",
                }, 
            });
        }
        /*
        let pathImagen = path.resolve(__dirname, `../public/assets/uploads/peliculas/${imageAnterior}`)
        console.log(pathImagen)
        if(fs.existsSync(pathImagen)){
            fs.unlinkSync(pathImagen)
        }*/
        
        usuarioDB.image = nombreArchivo;
        usuarioDB.titulo= titulo
        usuarioDB.descripcion = descripcion
        usuarioDB.categoria = categoria
        usuarioDB.tags = tags

        usuarioDB.save((err, usuarioGuardado) => {
        });
    });
}



module.exports = app
/*

app.put('/updateProfile/:id', verificaToken, async function(req, res){
    let id = req.params.id;
    let img = req.files.avatar;
    let { nombre, email, nameImg } = req.body;
    let nombreArchivoCortado = img.name.split('.');
    let extencion = nombreArchivoCortado[nombreArchivoCortado.length - 1];
    let extencionesValidas = ["png", "jpg", "jpeg"];
    if (extencionesValidas.indexOf(extencion) < 0) {
        res.redirect("/profile");
    } else {
        let nombreArchivo = `${id}-${new Date().getMilliseconds()}.${extencion}`

        img.mv(`public/assets/img/${nombreArchivo}`, (err) => {
            if (err) {
                res.redirect("/profile");
            }
            let imagenS3 = path.resolve(__dirname, `../public/assets/img/${nombreArchivo}`);
        
            fs.readFile( imagenS3, function (err, data) {
                if (err) {
                    console.log('Ocurrio un error:', err)
                    res.redirect('/profile')
                } // Something went wrong!
                else {
                    const params = uploadParams;
                    uploadParams.Key = nombreArchivo; //aqui va el nombre del archivo
                    uploadParams.Body = data; //aqui va el path osea la ruta del archive pes
                    s3Client.upload(params, async (err, data) => {
                        if (err) {
                            res.status(500).json({error:"Error -> " + err});
                        } else {
                            await Usuario.findByIdAndUpdate(req.params.id, { nombre: nombre, email: email, img: data.Location });
                            res.redirect("/profile");
                        }
                        fs.unlink(imagenS3, (err) => {
                            if (err) {
                                console.log('Error de remover: ', err.message)
                            }
                        })
                    });
                }
            })
        })
        
    }
});
*/
