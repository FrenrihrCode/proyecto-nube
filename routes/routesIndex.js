const express = require('express');
const router = express.Router();
const Contacto = require('../models/contacto');

router.get("/", function (req, res) {
    res.render('index')
});
router.get("/about", function (req, res) {
    res.render('about')
});
router.get("/contact", function (req, res) {
    res.render('contact')
});
router.post("/request", async function (req, res) {
    var name = req.body.name
    var email = req.body.email
    var date = req.body.date
    var telf = req.body.telf
    var message = req.body.message

    let newContacto = new Contacto({name, email, date, telf, message});
    await newContacto.save(function(err){
        if(err) console.log(err);
    })
    res.render('request', {
        name: name,
        email: email,
        date: date,
        telf: telf,
        message: message,
    })
});
module.exports = router